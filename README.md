![Sample-Code](https://gitlab.com/softbutterfly/open-source/open-source-office/-/raw/master/banners/softbutterfly-open-source--banner--sample-code.png)
# New Relic Demo: Logs

Entornos demostrativas para monitoreo con New Relic Logs

- [Fluentd (Centos 6.10)][fluent-centos-6.10]

[fluent-centos-6.10]: ./fluentd-on-centos6.10/README.md
