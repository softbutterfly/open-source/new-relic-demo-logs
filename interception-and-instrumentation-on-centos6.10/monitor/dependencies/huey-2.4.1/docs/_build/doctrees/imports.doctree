��!5      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�.. _imports:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��imports�u�tagname�h	�line�K�parent�hhh�source��(/home/charles/code/huey/docs/imports.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�$Understanding how tasks are imported�h]�h �Text����$Understanding how tasks are imported�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �	paragraph���)��}�(hX  Behind-the-scenes when you decorate a function with :py:meth:`~Huey.task` or
:py:meth:`~Huey.periodic_task`, the function registers itself with an in-memory
registry. When a task function is called, a reference is put into the queue,
along with the arguments the function was called with, etc. The message is then
read by the consumer, and the task function is looked-up in the consumer's
registry.  Because of the way this works, it is strongly recommended
that **all decorated functions be imported when the consumer starts up**.�h]�(h.�4Behind-the-scenes when you decorate a function with �����}�(h�4Behind-the-scenes when you decorate a function with �hh;hhh NhNub�sphinx.addnodes��pending_xref���)��}�(h�:py:meth:`~Huey.task`�h]�h �literal���)��}�(hhIh]�h.�task()�����}�(hhhhMubah}�(h]�h]�(�xref��py��py-meth�eh]�h]�h]�uhhKhhGubah}�(h]�h]�h]�h]�h]��reftype��meth��	refdomain�hX�refexplicit���	py:module�N�py:class�N�	reftarget��	Huey.task��refdoc��imports��refwarn��uhhEh h!hKhh;ubh.� or
�����}�(h� or
�hh;hhh NhNubhF)��}�(h�:py:meth:`~Huey.periodic_task`�h]�hL)��}�(hhuh]�h.�periodic_task()�����}�(hhhhwubah}�(h]�h]�(hW�py��py-meth�eh]�h]�h]�uhhKhhsubah}�(h]�h]�h]�h]�h]��reftype��meth��	refdomain�h��refexplicit��hgNhhNhi�Huey.periodic_task�hkhlhm�uhhEh h!hKhh;ubh.Xf  , the function registers itself with an in-memory
registry. When a task function is called, a reference is put into the queue,
along with the arguments the function was called with, etc. The message is then
read by the consumer, and the task function is looked-up in the consumer’s
registry.  Because of the way this works, it is strongly recommended
that �����}�(hXd  , the function registers itself with an in-memory
registry. When a task function is called, a reference is put into the queue,
along with the arguments the function was called with, etc. The message is then
read by the consumer, and the task function is looked-up in the consumer's
registry.  Because of the way this works, it is strongly recommended
that �hh;hhh NhNubh �strong���)��}�(h�C**all decorated functions be imported when the consumer starts up**�h]�h.�?all decorated functions be imported when the consumer starts up�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh;ubh.�.�����}�(h�.�hh;hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh �note���)��}�(h�QIf a task is not recognized, the consumer will raise a
:py:class:`HueyException`.�h]�h:)��}�(h�QIf a task is not recognized, the consumer will raise a
:py:class:`HueyException`.�h]�(h.�7If a task is not recognized, the consumer will raise a
�����}�(h�7If a task is not recognized, the consumer will raise a
�hh�ubhF)��}�(h�:py:class:`HueyException`�h]�hL)��}�(hh�h]�h.�HueyException�����}�(hhhh�ubah}�(h]�h]�(hW�py��py-class�eh]�h]�h]�uhhKhh�ubah}�(h]�h]�h]�h]�h]��reftype��class��	refdomain�hΌrefexplicit��hgNhhNhi�HueyException�hkhlhm�uhhEh h!hKhh�ubh.�.�����}�(hh�hh�ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh$hhh h!hNubh:)��}�(hX  The consumer is executed with a single required parameter -- the import path to
a :py:class:`Huey` object.  It will import the Huey instance along with
anything else in the module -- thus you must be sure **imports of your tasks
occur with the import of the Huey object**.�h]�(h.�SThe consumer is executed with a single required parameter – the import path to
a �����}�(h�RThe consumer is executed with a single required parameter -- the import path to
a �hh�hhh NhNubhF)��}�(h�:py:class:`Huey`�h]�hL)��}�(hh�h]�h.�Huey�����}�(hhhh�ubah}�(h]�h]�(hW�py��py-class�eh]�h]�h]�uhhKhh�ubah}�(h]�h]�h]�h]�h]��reftype��class��	refdomain�j  �refexplicit��hgNhhNhi�Huey�hkhlhm�uhhEh h!hKhh�ubh.�l object.  It will import the Huey instance along with
anything else in the module – thus you must be sure �����}�(h�k object.  It will import the Huey instance along with
anything else in the module -- thus you must be sure �hh�hhh NhNubh�)��}�(h�B**imports of your tasks
occur with the import of the Huey object**�h]�h.�>imports of your tasks
occur with the import of the Huey object�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh.�.�����}�(hh�hh�hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh#)��}�(hhh]�(h()��}�(h�Suggested organization of code�h]�h.�Suggested organization of code�����}�(hj7  hj5  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj2  hhh h!hKubh:)��}�(h�\Generally, I structure things like this, which makes it very easy to avoid
circular imports.�h]�h.�\Generally, I structure things like this, which makes it very easy to avoid
circular imports.�����}�(hjE  hjC  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhj2  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h��``config.py``, the module containing the :py:class:`Huey` object.

.. code-block:: python

    # config.py
    from huey import RedisHuey

    huey = RedisHuey('testing')
�h]�(h:)��}�(h�A``config.py``, the module containing the :py:class:`Huey` object.�h]�(hL)��}�(h�``config.py``�h]�h.�	config.py�����}�(hhhj`  ubah}�(h]�h]�h]�h]�h]�uhhKhj\  ubh.�, the module containing the �����}�(h�, the module containing the �hj\  ubhF)��}�(h�:py:class:`Huey`�h]�hL)��}�(hju  h]�h.�Huey�����}�(hhhjw  ubah}�(h]�h]�(hW�py��py-class�eh]�h]�h]�uhhKhjs  ubah}�(h]�h]�h]�h]�h]��reftype��class��	refdomain�j�  �refexplicit��hgNhhNhi�Huey�hkhlhm�uhhEh h!hKhj\  ubh.� object.�����}�(h� object.�hj\  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhjX  ubh �literal_block���)��}�(h�C# config.py
from huey import RedisHuey

huey = RedisHuey('testing')�h]�h.�C# config.py
from huey import RedisHuey

huey = RedisHuey('testing')�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve��language��python��linenos���highlight_args�}�uhj�  h h!hKhjX  ubeh}�(h]�h]�h]�h]�h]�uhjV  hjS  hhh NhNubjW  )��}�(h��``tasks.py``, the module containing any decorated functions.  Imports the
``huey`` object from the ``config.py`` module:

.. code-block:: python

    # tasks.py
    from config import huey

    @huey.task()
    def add(a, b):
        return a + b
�h]�(h:)��}�(h�x``tasks.py``, the module containing any decorated functions.  Imports the
``huey`` object from the ``config.py`` module:�h]�(hL)��}�(h�``tasks.py``�h]�h.�tasks.py�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhKhj�  ubh.�>, the module containing any decorated functions.  Imports the
�����}�(h�>, the module containing any decorated functions.  Imports the
�hj�  ubhL)��}�(h�``huey``�h]�h.�huey�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhKhj�  ubh.� object from the �����}�(h� object from the �hj�  ubhL)��}�(h�``config.py``�h]�h.�	config.py�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhKhj�  ubh.� module:�����}�(h� module:�hj�  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK&hj�  ubj�  )��}�(h�P# tasks.py
from config import huey

@huey.task()
def add(a, b):
    return a + b�h]�h.�P# tasks.py
from config import huey

@huey.task()
def add(a, b):
    return a + b�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�j�  j�  j�  �python�j�  �j�  }�uhj�  h h!hK)hj�  ubeh}�(h]�h]�h]�h]�h]�uhjV  hjS  hhh NhNubjW  )��}�(hX�  ``main.py`` / ``app.py``, the "main" module.  Imports both the ``config.py``
module **and** the ``tasks.py`` module.

.. code-block:: python

    # main.py
    from config import huey  # import the "huey" object.
    from tasks import add  # import any tasks / decorated functions


    if __name__ == '__main__':
        result = add(1, 2)
        print('1 + 2 = %s' % result.get(blocking=True))
�h]�(h:)��}�(h�t``main.py`` / ``app.py``, the "main" module.  Imports both the ``config.py``
module **and** the ``tasks.py`` module.�h]�(hL)��}�(h�``main.py``�h]�h.�main.py�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhKhj  ubh.� / �����}�(h� / �hj  ubhL)��}�(h�
``app.py``�h]�h.�app.py�����}�(hhhj1  ubah}�(h]�h]�h]�h]�h]�uhhKhj  ubh.�+, the “main” module.  Imports both the �����}�(h�', the "main" module.  Imports both the �hj  ubhL)��}�(h�``config.py``�h]�h.�	config.py�����}�(hhhjD  ubah}�(h]�h]�h]�h]�h]�uhhKhj  ubh.�
module �����}�(h�
module �hj  ubh�)��}�(h�**and**�h]�h.�and�����}�(hhhjW  ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh.� the �����}�(h� the �hj  ubhL)��}�(h�``tasks.py``�h]�h.�tasks.py�����}�(hhhjj  ubah}�(h]�h]�h]�h]�h]�uhhKhj  ubh.� module.�����}�(h� module.�hj  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK2hj  ubj�  )��}�(h��# main.py
from config import huey  # import the "huey" object.
from tasks import add  # import any tasks / decorated functions


if __name__ == '__main__':
    result = add(1, 2)
    print('1 + 2 = %s' % result.get(blocking=True))�h]�h.��# main.py
from config import huey  # import the "huey" object.
from tasks import add  # import any tasks / decorated functions


if __name__ == '__main__':
    result = add(1, 2)
    print('1 + 2 = %s' % result.get(blocking=True))�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  j�  �python�j�  �j�  }�uhj�  h h!hK5hj  ubeh}�(h]�h]�h]�h]�h]�uhjV  hjS  hhh NhNubeh}�(h]�h]�h]�h]�h]��bullet��*�uhjQ  h h!hKhj2  hhubh:)��}�(h��To run the consumer, point it at ``main.huey``, in this way, both the ``huey``
instance **and** the task functions are imported in a centralized location.�h]�(h.�!To run the consumer, point it at �����}�(h�!To run the consumer, point it at �hj�  hhh NhNubhL)��}�(h�``main.huey``�h]�h.�	main.huey�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhKhj�  ubh.�, in this way, both the �����}�(h�, in this way, both the �hj�  hhh NhNubhL)��}�(h�``huey``�h]�h.�huey�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhKhj�  ubh.�

instance �����}�(h�

instance �hj�  hhh NhNubh�)��}�(h�**and**�h]�h.�and�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�; the task functions are imported in a centralized location.�����}�(h�; the task functions are imported in a centralized location.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK@hj2  hhubj�  )��}�(h�$ huey_consumer.py main.huey�h]�h.�$ huey_consumer.py main.huey�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  j�  �console�j�  �j�  }�uhj�  h h!hKChj2  hhubeh}�(h]��suggested-organization-of-code�ah]�h]��suggested organization of code�ah]�h]�uhh"hh$hhh h!hKubeh}�(h]�(�$understanding-how-tasks-are-imported�heh]�h]�(�$understanding how tasks are imported��imports�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�j  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j1  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(j  hj  j  j�  j�  u�	nametypes�}�(j  �j  Nj�  Nuh}�(hh$j  h$j�  j2  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h:)��}�(hhh]�h.�-Hyperlink target "imports" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj�  uba�transformer�N�
decoration�Nhhub.