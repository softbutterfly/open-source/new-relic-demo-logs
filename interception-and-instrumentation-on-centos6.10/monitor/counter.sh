#! /bin/env bash
datafile=./data/Limsp_act_0.log
timestamps=$(cat ${datafile} | awk '{print $1 " " $2}')

index=0
currentTimestamp=""

IN_APNSOUT=0
IN_GOOGLE_FCM_OUT=0
IN_COLHTTP_OUT1=0
IN_COLHTTP_OUT2=0
IN_COLHTTP_OUT3=0
IN_COLHTTP_OUT4=0
IN_SPARKPOST_OUT=0

SEND_APNSOUT=0
SEND_GOOGLE_FCM_OUT=0
SEND_COLHTTP_OUT1=0
SEND_COLHTTP_OUT2=0
SEND_COLHTTP_OUT3=0
SEND_COLHTTP_OUT4=0
SEND_SPARKPOST_OUT=0

for timestampPart in $timestamps; do
    index=$(( (index + 1) % 2))

    if (( $index < 1 )); then
        timestamp="${timestamp} ${timestampPart}"
        timestamp=$(echo $timestamp | sed 's/\[/\\[/g' | sed 's/\]/\\]/g')

        if [ ! "$timestamp" = "$currentTimestamp" ]; then
            currentTimestamp=$timestamp
            echo -e $timestamp
            echo "IN apnsout         $(grep -E "${currentTimestamp} +IN +apnsout" ${datafile} | wc -l)"
            echo "IN google-fcm-out  $(grep -E "${currentTimestamp} +IN +google-fcm-out" ${datafile} | wc -l)"
            echo "IN colhttp-out1    $(grep -E "${currentTimestamp} +IN +colhttp-out1" ${datafile} | wc -l)"
            echo "IN colhttp-out2    $(grep -E "${currentTimestamp} +IN +colhttp-out2" ${datafile} | wc -l)"
            echo "IN colhttp-out3    $(grep -E "${currentTimestamp} +IN +colhttp-out3" ${datafile} | wc -l)"
            echo "IN colhttp-out4    $(grep -E "${currentTimestamp} +IN +colhttp-out4" ${datafile} | wc -l)"
            echo "IN sparkpost-out   $(grep -E "${currentTimestamp} +IN +sparkpost-out" ${datafile} | wc -l)"

            echo "SEND apnsout         $(grep -E "${currentTimestamp} +SEND +apnsout" ${datafile} | wc -l)"
            echo "SEND google-fcm-out  $(grep -E "${currentTimestamp} +SEND +google-fcm-out" ${datafile} | wc -l)"
            echo "SEND colhttp-out1    $(grep -E "${currentTimestamp} +SEND +colhttp-out1" ${datafile} | wc -l)"
            echo "SEND colhttp-out2    $(grep -E "${currentTimestamp} +SEND +colhttp-out2" ${datafile} | wc -l)"
            echo "SEND colhttp-out3    $(grep -E "${currentTimestamp} +SEND +colhttp-out3" ${datafile} | wc -l)"
            echo "SEND colhttp-out4    $(grep -E "${currentTimestamp} +SEND +colhttp-out4" ${datafile} | wc -l)"
            echo "SEND sparkpost-out   $(grep -E "${currentTimestamp} +SEND +sparkpost-out" ${datafile} | wc -l)"
        fi
    else
        timestamp="$timestampPart"
    fi
done
